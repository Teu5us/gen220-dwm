/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>
#include "movestack.c"

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 10;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
/*  Display modes of the tab bar: never shown, always shown, shown only in  */
/*  monocle mode in the presence of several windows.                        */
/*  Modes after showtab_nmodes are disabled.                                */
enum showtab_modes { showtab_never, showtab_auto, showtab_nmodes, showtab_always};
static const int showtab			= showtab_auto;        /* Default tab bar show mode */
static const int toptab				= False;               /* False means bottom tab bar */

static const char *fonts[]          = { "sans:size=10" };
static const char dmenufont[]       = "sans:size=10";
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask		iscentered     isfloating   monitor */
	// { "Gimp",     NULL,       NULL,       0,            1,           -1 },
	// { "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
	{ NULL, "st", "agopener1",         0, 1, 1, -1 },
	{ NULL, "st", "pulsemixer",        0, 1, 1, -1 },
	{ NULL, "st", "mousemove_mode.sh", 0, 1, 1, -1 },
	{ NULL, "st", "mtran",             0, 1, 1, -1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,           	KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = { "st", "-t", scratchpadname, "-g", "100x30", "-e", "tmuxdd", NULL };

#include "nextprevtag.c"
#include "selfrestart.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,             		XK_apostrophe, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_s,      togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,             XK_w,      tabmode,        {-1} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,      rotatestack,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      rotatestack,     {.i = -1 } },
  	{ MODKEY|ControlMask,           XK_j,      movestack,      {.i = +1 } },
  	{ MODKEY|ControlMask,           XK_k,      movestack,      {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY|ShiftMask,             XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,              		XK_h,      view_adjacent,  { .i = -1 } },
	{ MODKEY,              		XK_l,      view_adjacent,  { .i = +1 } },
	{ MODKEY|Mod1Mask,             XK_h,      setcfact,       {.f = +0.25} },
	{ MODKEY|Mod1Mask,             XK_l,      setcfact,       {.f = -0.25} },
	{ MODKEY|Mod1Mask,             XK_o,      setcfact,       {.f =  0.00} },
	{ MODKEY|ControlMask,           XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,             		XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,             XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,             		XK_f,      togglefullscr,     {0} },
	{ MODKEY,                       XK_u,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_o,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	// { MODKEY,                       XK_parenright,      view,           {.ui = ~0 } },
	// { MODKEY|ShiftMask,             XK_parenright,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ControlMask,           XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ControlMask,           XK_period, tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_F5,     xrdb,           {.v = NULL } },
	{ MODKEY|ControlMask,           XK_F3,     spawn,          SHCMD("st -e mtran pe -l $(xsel)") },
	{ MODKEY|ControlMask,           XK_F4,     spawn,          SHCMD("st -e mtran pg -l $(xsel)") },
	{ MODKEY|ShiftMask,       	XK_z,      spawn,          SHCMD("emacs") },
	{ MODKEY,       	  	XK_c,      spawn,          SHCMD("clipmenu") },
	{ MODKEY,       	  	XK_grave,  spawn,          SHCMD("dmenuunicode") },
	{ MODKEY,       	        XK_a,      spawn,          SHCMD("xkb-switch -n; refbar") },
	{ MODKEY,       	        XK_e,      spawn,          SHCMD("ecl") },
	{ MODKEY|ControlMask,       	XK_e,      spawn,          SHCMD("telegram-desktop") },
	{ MODKEY|ShiftMask,       	XK_a,      spawn,          SHCMD("st -e pulsemixer") },
	// { MODKEY|ControlMask,       	XK_a,      spawn,          SHCMD("st -e agopener1") },
	{ MODKEY|ControlMask,       	XK_a,      spawn,          SHCMD("dmenuopener") },
	{ MODKEY,       	        XK_r,      spawn,          SHCMD("st -e ranger") },
	{ MODKEY,       	        XK_y,      spawn,          SHCMD("st -e youtube-viewer") },
	{ MODKEY|ShiftMask,       	XK_i,      spawn,          SHCMD("st -e htop") },
	{ MODKEY,       	        XK_w,      spawn,          SHCMD("firefox") },
	{ MODKEY|ControlMask,       	XK_w,      spawn,          SHCMD("st -e sudo nmtui") },
	{ MODKEY,     			XK_x,      spawn,          SHCMD("xkb-switch -s 'us(altgr-intl)'; refbar; lockscreen") },
	{ MODKEY|ShiftMask,     	XK_x,      spawn,          SHCMD("prompt 'Shutdown?' 'poweroff'") },
	{ MODKEY|ControlMask,   	XK_x,      spawn,          SHCMD("prompt 'Reboot?' 'reboot'") },
	{ MODKEY,       	        XK_equal,  spawn,          SHCMD("lmc up 5; refbar") },
	{ MODKEY|ShiftMask,             XK_equal,  spawn,          SHCMD("lmc up 15; refbar") },
	{ MODKEY,       	        XK_minus,  spawn,          SHCMD("lmc down 5; refbar") },
	{ MODKEY|ShiftMask,             XK_minus,  spawn,          SHCMD("lmc down 15; refbar") },
	{ MODKEY,       	        XK_F3,     spawn,          SHCMD("displayselect") },
	{ MODKEY,       	        XK_F4,     spawn,          SHCMD("prompt 'Suspend?' 'systemctl suspend'") },
	{ MODKEY,       	        XK_F5,     spawn,          SHCMD("sudo systemctl restart NetworkManager") },
	{ MODKEY,       	        XK_F6,     spawn,          SHCMD("st -e transmission-remote-cli") },
	{ MODKEY,       	        XK_F7,     spawn,          SHCMD("td-toggle") },
	{ MODKEY,       	        XK_F8,     spawn,          SHCMD("tpad") },
	{ MODKEY,       	        XK_F9,     spawn,          SHCMD("dmenumount") },
	{ MODKEY,       	        XK_F10,    spawn,          SHCMD("dmenuumount") },
	{ MODKEY,       	        XK_F11,    spawn,          SHCMD("ducksearch") },
	{ MODKEY,       	        XK_F12,    spawn,          SHCMD("prompt 'Hibernate?' 'systemctl hibernate'") },
	{ MODKEY,       		XK_Insert, spawn,          SHCMD("showclip") },
	{ MODKEY,       		XK_Pause,  spawn,          SHCMD("xcqr") },
	{ 0,       			XK_Print,  spawn,          SHCMD("screenshot") },
	{ ShiftMask,       		XK_Print,  spawn,          SHCMD("maimpick") },
	{ MODKEY,       		XK_Print,  spawn,          SHCMD("dmenurecord") },
	{ MODKEY,       		XK_Delete, spawn,          SHCMD("stoprec") },
	{ MODKEY|ControlMask,   	XK_F2,     spawn,          SHCMD("st -e Trans") },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	// TAGKEYS(                        XK_exclam,                      0)
	// TAGKEYS(                        XK_at,                      1)
	// TAGKEYS(                        XK_numbersign,                      2)
	// TAGKEYS(                        XK_dollar,                      3)
	// TAGKEYS(                        XK_percent,                      4)
	// TAGKEYS(                        XK_asciicircum,                      5)
	// TAGKEYS(                        XK_ampersand,                      6)
	// TAGKEYS(                        XK_asterisk,                      7)
	// TAGKEYS(                        XK_parenleft,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ 0,       			XF86XK_AudioRaiseVolume,      spawn,          SHCMD("lmc up 5; refbar") },
	{ 0,       			XF86XK_AudioLowerVolume,      spawn,          SHCMD("lmc down 5; refbar") },
	{ 0,       			XF86XK_AudioMute,      	      spawn,          SHCMD("lmc mute; refbar") },
	{ ShiftMask,       			XF86XK_AudioRaiseVolume,      spawn,          SHCMD("lmc up 10; refbar") },
	{ ShiftMask,       			XF86XK_AudioLowerVolume,      spawn,          SHCMD("lmc down 10; refbar") },
	{ 0,       			XF86XK_AudioMicMute,          spawn,          SHCMD("sbuckle") },
	{ 0,       			XF86XK_PowerOff,      	      spawn,          SHCMD("prompt 'Shutdown?' 'poweroff'") },
	{ 0,       			XF86XK_Display,      	      spawn,          SHCMD("arandr") },
	{ 0,       			XF86XK_Sleep,      	      spawn,          SHCMD("prompt 'Suspend?' 'systemctl suspend'") },
	{ MODKEY|ShiftMask,             XK_Escape,      self_restart,   {0} },
	{ 0,       	XF86XK_MonBrightnessUp,       spawn,          SHCMD("xbacklight -inc 10") },
	{ 0,       	XF86XK_MonBrightnessDown,     spawn,          SHCMD("xbacklight -dec 10") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkTabBar,            0,              Button1,        focuswin,       {0} },
};
